import datetime

from setuptools import setup

TIMESTAMP = str(datetime.datetime.now().replace(microsecond=0).isoformat()).\
    replace('-', '').replace('T', '').replace(':', '')

LONG_DESCRIPTION = \
    open('README.rst').read() + '\n\n' + \
    open('CHANGES.txt').read() + '\n\n' + \
    open('AUTHORS.txt').read()

setup(
    name='mosaik.HDF5_SemVer',
    version='0.4.0' + 'rc' + TIMESTAMP,
    author='Stefan Scherfke',
    author_email='bengt.lueers@gmail.com',
    description=('Stores mosaik simulation data in an HDF5 database.'),
    long_description=LONG_DESCRIPTION,
    url='https://bitbucket.org/mosaik/mosaik-hdf5',
    install_requires=[
        'h5py>=2.2.1',
        'mosaik.API-SemVer>=2.4.2rc20201202043229',
        'networkx>=1.9',
        'numpy>=1.8.1',
    ],
    py_modules=['mosaik_hdf5'],
    include_package_data=True,
    maintainer='Bengt Lüers',
    maintainer_email='bengt.lueers@gmail.com',
    entry_points={
        'console_scripts': [
            'mosaik-hdf5 = mosaik_hdf5:main',
        ],
    },
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU Lesser General Public License v2 (LGPLv2)',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Topic :: Scientific/Engineering',
    ],
)
